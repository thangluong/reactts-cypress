/// <reference types="Cypress" />
before(() => {
  cy.visit("/");
});

context("Home page", () => {
  it("Learn React text exist", () => {
    cy.get("a").should("contain", "Learn React");
  });

  it("Learn React Typescript not exist", () => {
    cy.get("a").should("not.contain", "Learn React Typescript");
  });
});

context("Change page", () => {
  it("Click Learn React", () => {
    cy.contains("Learn React").invoke("removeAttr", "target").click();
    cy.url().should("contain", "/ok");
  });
});
